## Basic OOP

1. ###### Public & Private

   Usually declared public:

   ​	Constructors and methods; Static constants

   Usually declared private:

   ​	Helper methods; Fields

2. ###### Accessors & Modifiers 访问器和修改器

   to set or return the values of private field

   `public String get(){`

      `	   return xxxx;`

   `}`

   `public String set(){`

      `	   this.xxxx = xxxx;`

   `}`

3. ###### Encapsulation 封装

   to guarantee a change in one class does not affect other classes

4. ###### this

   - Refer to a field

     `this.field;`

   - Call a method

     `this.method(parameters);`

   - One constructor can call another

     `this(parameters);`

5. ###### Constructors

   Constructors of a class can call each other using the keyword “this” (referred to as constructor chaining) – a good way to avoid duplicating code.

   - Constructors have the same name as the class
   - Each class can have one or more constructors
   - Each constructors can have 0, 1 or more parameters
   - There  is no return value in constructor
   - Always come along with "new" operation
   - there is a default constructor in each java file

   More details for Constructors:

   [CSDN](https://blog.csdn.net/qq_33642117/article/details/51909346)

   [cnblogs](https://www.cnblogs.com/mingsay/p/12441490.html)

   

6. ###### Static Fields 静态域

   - A static field is shared by all objects of the class
   - A static field is not initialised in construstor 
   - It usually initialised in declarations/ public static methods/ just use default value
   - Static methods can access and manipulate class’s static fields
   - Static methods cannot access instance fields or call instance methods of the class

7. ###### Java Collection Framework

   ![](https://gitlab.com/unnc-fse-20126314/developing-maintainable-software/-/raw/master/image_resource/123.gif)

   - TreeSet class

     ![](https://gitlab.com/unnc-fse-20126314/developing-maintainable-software/-/raw/master/image_resource/456.jpg)

   - HashMap class

     ![](https://gitlab.com/unnc-fse-20126314/developing-maintainable-software/-/raw/master/image_resource/789.jpg)

8. ######  Aggregation & Composition

   - Aggrehration
   
     <img src="https://gitlab.com/unnc-fse-20126314/developing-maintainable-software/-/raw/master/image_resource/aggregation.jpg" style="zoom:33%;" />
   
   - Composition
   
     <img src="https://gitlab.com/unnc-fse-20126314/developing-maintainable-software/-/raw/master/image_resource/composition.jpg" style="zoom:33%;" />
   
9. 

   

