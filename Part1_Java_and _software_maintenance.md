## Basic Maintenance

1. ###### Use packages 

   to prevent naming conflicts

   Example: Main.java & Employee.java are in different packages

   ​				 `import <the path to Employee.java>.Employee;` #引入相关文件

   ​				 `Employee e1  =new Employee( )`; #通过对象实例化跨包调用其他类的方法

   ​				 

2. ###### Javadocs

   to help other developers quickly understand the certain code even the whole project

   以 IntelliJ IDEA 为例， click “Tools” --- click "Generate JavaDoc..." to add the javadocs

   More Details for JavaDoc:

   [CSDN](https://blog.csdn.net/vbirdbest/article/details/80296136)

   

   